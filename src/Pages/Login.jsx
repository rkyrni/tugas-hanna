// import React from "react";

// const Login = () => {
//   return <div>Login</div>;
// };

// export default Login;
import React, { useContext, useState } from "react";
import axios from "axios";
import Cookies from "js-cookie";
import {
  HiOutlineEnvelopeOpen,
  HiOutlineLockClosed,
  HiUser,
} from "react-icons/hi2";
import { Link, useNavigate } from "react-router-dom";
import Background from "../assets/img/bg-1.jpg";
// import Loading from "../../components/Loading";
// import { GlobalManagementContext } from "../../context";

const Login = () => {
  const [input, setInput] = useState({
    username: "",
    password: "",
  });
  const [isLoading, setIsLoading] = useState(false);
  const [textHeader, setTextHeader] = useState("Login Form");
  //   const { setUserDetails } = useContext(GlobalManagementContext);

  const navigate = useNavigate();

  const handlerChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  //   const handlerSubmit = async (e) => {
  //     e.preventDefault();
  //     setIsLoading(true);
  //     try {
  //       const response = await axios.post(
  //         `https://dummyjson.com/auth/login`,
  //         input
  //       );
  //       const { email, firstName, gender, lastName, username, token, id, image } =
  //         response.data;
  //       setUserDetails({ email, firstName, lastName, gender, username, image });
  //       Cookies.set("data", JSON.stringify(response.data));
  //       Cookies.set("token", token);
  //       Cookies.set("id", id);
  //       navigate("/home");
  //       setIsLoading(false);
  //     } catch (error) {
  //       setTextHeader("Username/Password incorrect!");
  //       setIsLoading(false);
  //     }
  //   };
  const handlerSubmit = (e) => {
    e.preventDefault();
    if (
      input.username.toLocaleLowerCase() === "hanna" &&
      input.password === "123456"
    ) {
      navigate("/dashboard");
    } else {
      setTextHeader("username/password salah!");
    }
  };

  return (
    <div
      style={{ backgroundImage: `url(${Background})` }}
      className="flex justify-center min-h-screen bg-cover bg-bottom text-slate-300"
    >
      {/* {isLoading ? <Loading /> : null} */}
      <div
        style={{
          boxShadow: "0px 0px 25px 10px black",
          background: "rgba(4, 29, 23, 0.5)",
        }}
        className="self-center py-3 px-8 rounded-lg flex flex-col gap-5 lg:w-1/4 w-3/4"
      >
        <div className="flex justify-center -mt-8">
          <div
            style={{ boxShadow: "0px 0px 6px 5px black" }}
            className="w-max p-3 rounded-full bg-black text-white"
          >
            <HiUser />
          </div>
        </div>
        <h2 className="text-2xl text-center font-bold mb-6">{textHeader}</h2>
        <form
          className="flex flex-col gap-6 pb-10"
          action=""
          method="post"
          onSubmit={handlerSubmit}
        >
          <div className="flex gap-3 border-b-2 border-slate-700 pb-1">
            <span className="self-center">
              <HiOutlineEnvelopeOpen />
            </span>
            <input
              className="bg-transparent"
              name="username"
              onChange={handlerChange}
              type="text"
              placeholder="username"
              value={input.username}
            />
          </div>
          <div className="flex gap-3 border-b-2 border-slate-700 pb-1">
            <span className="self-center">
              <HiOutlineLockClosed />
            </span>
            <input
              className="bg-transparent"
              name="password"
              onChange={handlerChange}
              type="password"
              placeholder="password"
              value={input.password}
            />
          </div>
          <button
            className="border border-slate-700 py-2 font-semibold hover:shadow-2xl hover:shadow-black hover:text-white"
            type="submit"
          >
            Login
          </button>
        </form>
      </div>
    </div>
  );
};

export default Login;
