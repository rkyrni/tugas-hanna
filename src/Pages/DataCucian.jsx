import React, { useEffect, useState } from "react";
import BaseLayout from "../layouts/BaseLayout";
import { IoChevronDown, IoSearch, IoStatsChart } from "react-icons/io5";
import { dataPesanan } from "../datas/data";
import Modal from "../Components/Modal";

const DataCucian = () => {
  const [filterData, setFilterData] = useState("ALL");
  const [dataCustomer, setDataCustomer] = useState([]);
  const [dataCustomerView, setDataCustomerView] = useState([]);
  const [inputSearch, setInputSearch] = useState("");
  const [modalDetail, setModalDetail] = useState(false);
  const [dataDetail, setDataDetail] = useState(null);

  useEffect(() => {
    const dt = dataPesanan;
    setDataCustomer(dt);

    if (filterData === "ALL") setDataCustomerView(dt);
    if (filterData === "EXPRESS") {
      const dtFilter = dt.filter(
        (item) => item.packageType.toUpperCase() === "EXPRESS"
      );
      setDataCustomerView(dtFilter);
    }
    if (filterData === "REGULAR") {
      const dtFilter = dt.filter(
        (item) => item.packageType.toUpperCase() === "REGULAR"
      );
      setDataCustomerView(dtFilter);
    }
  }, [filterData]);

  return (
    <BaseLayout>
      {modalDetail ? (
        <Modal
          setIsModalView={setModalDetail}
          body={
            <div>
              <p>Name: {dataDetail?.name}</p>
              <p>Address: {dataDetail?.address}</p>
              <p>Weight: {dataDetail?.weight}</p>
              <p>Package Type:{dataDetail?.packageType}</p>
              <p>Code: {dataDetail?.uniqueCode}</p>
              <p>Status: {dataDetail?.status}</p>
              <p>Payment: {dataDetail?.isPayment ? "Lunas" : "Belum Bayar"}</p>
            </div>
          }
        />
      ) : null}
      <div className="h-full flex flex-col gap-3">
        <div className="font-bold text-xl flex gap-2">
          <span className="self-center">
            <IoStatsChart />
          </span>
          <span>Laundry Data</span>
        </div>
        <div className="bg-white rounded-lg h-full">
          {/* =========== */}
          <div>
            <div className="p-5 flex justify-end">
              <input
                type="search"
                name="search"
                value={inputSearch}
                onChange={(e) => setInputSearch(e.target.value)}
                className="border border-[#07f57a] px-2 bg-slate-200 rounded-l text-lg text-slate-800"
                placeholder="Search by code..."
              />
              <button className="bg-[#07f57a] px-2 text-black flex rounded-r text-lg">
                <span className="self-center">
                  <IoSearch />
                </span>
              </button>
            </div>
          </div>
          {/* =========== */}
          {/* =========== */}
          <div className="mb-5">
            <div className="flex gap-2 pl-5">
              <p className="self-center font-semibold">Package Type :</p>
              <div className="dropdown dropdown-bottom">
                <label
                  tabIndex={0}
                  className="m-1 bg-[#07f57a] px-3 py-1 font-bold rounded-lg flex gap-4 cursor-pointer"
                >
                  <span>{filterData}</span>
                  <span className="self-center">
                    <IoChevronDown />
                  </span>
                </label>
                <ul
                  tabIndex={0}
                  className="dropdown-content menu shadow bg-[#affae7] text-start rounded w-52"
                >
                  <button
                    onClick={() => setFilterData("ALL")}
                    className="hover:bg-[#48ab92] w-full py-2 text-start pl-2 font-bold mb-2"
                  >
                    ALL
                  </button>
                  <button
                    onClick={() => setFilterData("EXPRESS")}
                    className="hover:bg-[#48ab92] w-full py-2 text-start pl-2 font-bold mb-2"
                  >
                    EXPRESS
                  </button>
                  <button
                    onClick={() => setFilterData("REGULAR")}
                    className="hover:bg-[#48ab92] w-full py-2 text-start pl-2 font-bold"
                  >
                    REGULAR
                  </button>
                </ul>
              </div>
            </div>
          </div>
          {/* =========== */}
          {/* =========== */}
          <div className="px-5">
            <table className=" w-full border border-slate-200">
              <thead className="bg-[#07f57a]">
                <tr className="text-start">
                  <th className="">No</th>
                  <th className="text-start py-3">Name</th>
                  <th className="text-start py-3">Code</th>
                  <th className="text-start py-3">Package</th>
                  <th className="text-start py-3">Status</th>
                  <th className="text-start py-3">Action</th>
                </tr>
              </thead>
              <tbody>
                {dataCustomerView.length !== 0 ? (
                  <>
                    {dataCustomerView
                      .filter((x) =>
                        x.uniqueCode
                          .toLowerCase()
                          .includes(inputSearch.toLowerCase())
                      )
                      .map((item, i) => {
                        return (
                          <tr
                            key={i}
                            className={`text-start ${
                              i % 2 === 0 ? "bg-slate-200" : ""
                            } `}
                          >
                            <th className="py-2 font-semibold">{i + 1}</th>
                            <td className="py-2 font-semibold">{item.name}</td>
                            <td className="py-2 font-semibold">
                              {item.uniqueCode}
                            </td>
                            <td className="py-2 font-semibold">
                              {item.packageType}
                            </td>
                            <td className="py-2 font-semibold">
                              {item.status}
                            </td>
                            <td className="py-2 font-semibold">
                              <button
                                onClick={() => {
                                  const detail = dataCustomerView.find(
                                    (x) => x.uniqueCode === item.uniqueCode
                                  );
                                  setModalDetail(true);
                                  setDataDetail(detail);
                                }}
                                className="bg-[#48ab92] px-3 rounded hover:bg-[#07f57a] text-white"
                              >
                                detail
                              </button>
                            </td>
                          </tr>
                        );
                      })}
                  </>
                ) : null}
              </tbody>
            </table>
          </div>
          {/* =========== */}
        </div>
      </div>
    </BaseLayout>
  );
};

export default DataCucian;
