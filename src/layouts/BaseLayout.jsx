import React from "react";
import SideBar from "../Components/SideBar";

const BaseLayout = ({ children }) => {
  return (
    <div>
      <div className="flex bg-slate-200 min-h-screen gap-10 justify-between  transition duration-700 text-slate-800">
        <SideBar />
        <div className="w-full p-5">{children}</div>
      </div>
    </div>
  );
};

export default BaseLayout;
