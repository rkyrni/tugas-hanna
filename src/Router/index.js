import React from "react";
import {
  Routes,
  Route,
  HashRouter,
  BrowserRouter,
  Navigate,
} from "react-router-dom";
import Cookies from "js-cookie";
import Dashboard from "../Pages/Dashboard";
import TambahCucian from "../Pages/TambahCucian";
import DataCucian from "../Pages/DataCucian";
import DataPaket from "../Pages/DataPaket";
import DataLaporan from "../Pages/DataLaporan";
import ManagementContext from "../ContextManagement";
import Login from "../Pages/Login";

function Router() {
  //   const PrivateRoute = ({ content }) => {
  //     if (Cookies.get("token")) return content;
  //     else return <Navigate to="/home" />;
  //   };

  //   const HandlerLoginRegister = ({ content }) => {
  //     if (Cookies.get("token")) return <Navigate to="/home" />;
  //     else return content;
  //   };

  return (
    <HashRouter>
      <ManagementContext>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/form/add" element={<TambahCucian />} />
          <Route path="/data-cucian" element={<DataCucian />} />
          <Route path="/data-paket" element={<DataPaket />} />
          <Route path="/data-laporan" element={<DataLaporan />} />
        </Routes>
      </ManagementContext>
    </HashRouter>
  );
}

export default Router;
