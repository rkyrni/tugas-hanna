import {
  IoAddCircle,
  IoHome,
  IoStatsChart,
  IoShirt,
  IoAlbums,
  IoLogOutSharp,
} from "react-icons/io5";

const dataMenu = [
  {
    label: "Dashboard",
    link: "/dashboard",
    icon: <IoHome />,
  },
  {
    label: "Add Customer",
    link: "/form/add",
    icon: <IoAddCircle />,
  },
  {
    label: "Customer Data",
    link: "/data-cucian",
    icon: <IoShirt />,
  },
  {
    label: "Package",
    link: "/data-paket",
    icon: <IoAlbums />,
  },
  {
    label: "Laundry Data",
    link: "/data-laporan",
    icon: <IoStatsChart />,
  },
  {
    label: "Logout",
    link: "BTN-LOGOUT",
    icon: <IoLogOutSharp />,
  },
];

export { dataMenu };
