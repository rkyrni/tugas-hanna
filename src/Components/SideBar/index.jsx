import React, { useContext, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { IoCaretBack } from "react-icons/io5";
import { dataMenu } from "./dataMenu";
import { DataContext } from "../../ContextManagement";
import Modal from "../Modal";

const SideBar = () => {
  const [modalConfirmLogout, setModalConfirmLogout] = useState(false);
  const { sideBarCollapse, setSideBarCollapse } = useContext(DataContext);

  const DataMenu = ({ data }) => {
    const pathName = useLocation().pathname;
    const navigate = useNavigate();
    return (
      <>
        {data.map((item, i) => {
          let locationStyle = "";
          if (item.link === pathName) {
            locationStyle =
              "bg-[#4db399] border-r-4 border-b-4 border-[#07f57a]";
          }

          return (
            <button
              onClick={() => {
                if (item.link === "BTN-LOGOUT") {
                  navigate("/");
                  return;
                } else navigate(`${item.link}`);
              }}
              key={i}
              className={`${locationStyle} text-start flex ${
                sideBarCollapse ? "justify-center pl-0" : "pl-4"
              } gap-5 py-3 font-bold hover:bg-[#48ab92] hover:border-r-4 hover:border-[#07f57a] text-slate-800`}
            >
              <span className="self-center">{item.icon}</span>
              <span
                className={`${
                  sideBarCollapse ? "hidden" : ""
                } lg:text-base text-sm`}
              >
                {item.label}
              </span>
            </button>
          );
        })}
      </>
    );
  };

  return (
    <div
      className={`${
        sideBarCollapse ? "w-[5%]" : "w-[20%]"
      } text-black bg-[#affae7] relative transition-all duration-700`}
    >
      {modalConfirmLogout ? (
        <Modal
          setIsModalView={setModalConfirmLogout}
          footer={<button>haha</button>}
        />
      ) : null}
      <button
        onClick={() => {
          if (sideBarCollapse) setSideBarCollapse(false);
          else setSideBarCollapse(true);
        }}
        className="bg-[#07f57a] p-2 rounded-full flex absolute justify-center -right-3 top-12"
      >
        <span
          className={`self-center ${
            sideBarCollapse ? "rotate-180" : "rotate-0"
          } transition duration-700`}
        >
          <IoCaretBack />
        </span>
      </button>
      <div>
        {!sideBarCollapse ? (
          <div className="py-6 text-center">
            <span className="text-4xl font-bold text-[#166b56]">Hanna</span>
            <span className="font-semibold italic ">Loundry</span>
          </div>
        ) : (
          <div className="text-4xl font-bold text-[#166b56] py-6 text-center">
            H
          </div>
        )}
      </div>
      <div className="flex flex-col gap-1">
        <DataMenu data={dataMenu} />
      </div>
    </div>
  );
};

export default SideBar;
